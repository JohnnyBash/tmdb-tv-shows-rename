
import sys
import locale
import argparse
import requests
import json

def _debug( text : str , **kwargs: dict ) :
	"""
	Print debugging line if args.debug is true.
	str  text   : The string to print.
	dict kwargs : Built-in print's arguments.

	return None
	"""
	if "args" in vars() or "args" in globals() and args.debug == True :
		if kwargs :
			print( text, **kwargs )
		else :
			print( text )

def _getISO_639_1() :
	"""
	Return the system's language in ISO 639-1 format.

	return str
	"""
	language, encoding = locale.getdefaultlocale()
	iso_639_1 = language.replace( "_", "-" )

	return iso_639_1

def _parseArgs() :
	"""
	Return command arguments.

	return dict
	"""
	# Getting locale
	iso_639_1 = _getISO_639_1()

	# Defining args
	argParser = argparse.ArgumentParser()
	argParser.add_argument( "--debug", action="store_true", default = False, help = "Enable debugging output" )
	argParser.add_argument( "-k", "--api-key", metavar = "FILE", type = str, default = "api_key", help = "Path to file containing plain text TMDB API key. Default : api_key" )
	argParser.add_argument( "-l", "--language", metavar = "LANGUAGE", type = str, default = iso_639_1, help = "Pass a ISO 639-1 value to display translated data for the fields that support it. Default based on system's locale : \"%s\"." % iso_639_1 )
	argParser.add_argument( "-p", "--page", metavar = "[1-1000]", type = int, default = 1, help = "Specify which page to query. Default : 1" )
	argParser.add_argument( "-a", "--adult", action="store_true", default = False, help = "Choose whether to inlcude adult (pornography) content in the results. Default : disabled" )
	argParser.add_argument( "-y", "--year", metavar = "YEAR", type = int, default = None, help = "First air date year" )

	argParser.add_argument( "title", metavar = "TITLE", type = str, help = "First air date year" )

	# Parsing args
	args = argParser.parse_args()

	return args

def searchShow( apiKey : str, title : str, language : str = None, page : int = 1, adult : bool = False, year : int = None ) :
	"""
	Search for TV show using https://www.themoviedb.org API

	str  apiKey   : The TMDB API key
	str  title    : The TV show title to search
	str  language : The result language wanted in ISO 639-1 format. Default : None.
	int  page     : The page number to return. Default : 1.
	bool adult    : Inclut adult content in result. Default : False.
	int  year     : The release year of the TV show.

	return dict
	"""
	# https://developers.themoviedb.org/3/search/search-tv-shows
	query = "https://api.themoviedb.org/3/search/tv?"

	# Required
	query += "api_key=%s&query=%s" % ( apiKey, title )

	# Optional
	if language :
		query += "&language=%s" % language
	if page :
		query += "&page=%d" % page
	if adult :
		query += "&include_adult=true"
	else :
		query += "&include_adult=false"
	if year :
		query += "&first_air_date_year=%d" % year
	_debug( "Requesting %s..." % query )

	data = None
	r = requests.get( query )
	if r.status_code == 200 :
		data = json.loads( r.text )

	return data

def _getPopularity( jsonObj ) :
	if "popularity" in jsonObj :
		v = int( jsonObj["popularity"] )
	else :
		v = 0

	return v

def printData( data ) :
	print( "Found %s results" % data["total_results"])
	print( "Page %d/%d" % ( data["page"], data["total_pages"] ))

	data["results"].sort( key = _getPopularity, reverse = True )
	for result in data["results"] :
		name = None
		originalName = None
		originCountry = None
		showId = None
		year = None

		if "name" in result and result["name"] :
			name = result["name"].strip()

		if "original_name" in result and result["original_name"] :
			originalName = result["original_name"].strip()

		if "origin_country" in result and result["origin_country"] :
			originCountry = result["origin_country"]

		if "id" in result and result["id"] : 
			showId = int( result["id"] )

		if "first_air_date" in result and result["first_air_date"] :
			year = int( result["first_air_date"].split( "-" )[0] )

		if name and showId:
			out = "\n%s" % name
			if year :
				out += " (%d)" % year
			out += " :\n\t            id : %d" % showId
			if originalName :
				out += "\n\t original name : %s" % originalName
			if originCountry and len( originCountry ) > 0 :
				out += "\n\torigin country : %s" % originCountry[0].strip()
				for country in originCountry[1:] :
					out += ", %s" % country.strip()

			print( out )

if __name__ == "__main__" :
	errno = 0

	# Getting args
	if errno == 0 :
		args = _parseArgs()
		_debug( args )

	# Getting API key
	if errno == 0 :
		apiKey = None
		with open( args.api_key ) as keyFile :
			apiKey = keyFile.read()
		_debug( "key = %s" % apiKey )

	# Requesting API
	if errno == 0 :
		data = searchShow( apiKey, args.title, args.language, args.page, args.adult, args.year )
		if not data :
			errno = 1

	# Formatting data
	if errno == 0 :
		printData( data )

	# Exit
	sys.exit( errno )
